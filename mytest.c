#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include "smalloc.h"


#define SIZE 4096 * 64


/* My test for smalloc and sfree.
	This test case is interesting as it tests the smalloc(0) case
	Also, it ensures freelist stores blocks in increasing address order.*/

int main(void) {
    mem_init(SIZE);
    
    char *ptrs[20];
    int i;

    /* Call smalloc 4 times */
    
    for(i = 0; i < 7; i++) {
        int num_bytes = (i) * 10;
    
        ptrs[i] = smalloc(num_bytes);
        write_to_mem(num_bytes, ptrs[i], i);
    }
    printf("List of allocated blocks:\n");
    print_allocated();
    printf("List of free blocks:\n");
    print_free();
    printf("Contents of allocated memory:\n");
    print_mem();
    
    printf("freeing %p result = %d\n", ptrs[1], sfree(ptrs[1]));
	printf("freeing %p result = %d\n", ptrs[4], sfree(ptrs[4]));
	printf("freeing %p result = %d\n", ptrs[3], sfree(ptrs[3]));
    
    printf("List of allocated blocks:\n");
    print_allocated();
    printf("List of free blocks:\n");
    print_free();
    printf("Contents of allocated memory:\n");
    print_mem();

    mem_clean();
    return 0;
}
