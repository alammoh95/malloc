#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include "smalloc.h"



void *mem;
struct block *freelist;
struct block *allocated_list;
struct block *header; //Node to store head node
struct block *prev; //Node to store prev nodes
struct block *temp; //Temporary node for traversal
int done; //indicator to end iteration through lists
int counter; //indicate position in list

void *smalloc(unsigned int nbytes) {
	//Special case if user decides to smalloc(0)
	if (nbytes == 0){
		return NULL;
	}	
	header = freelist; // Save the reference to head node of freelist
	done = 0;
	counter = 0;
	//Search for suitable block
	while(done == 0){
		//Block greater than what we require
		if (nbytes < (*freelist).size){
			temp = malloc(sizeof(struct block));
			(*temp).addr = (*freelist).addr;
			(*temp).size = nbytes;
			(*temp).next = allocated_list;
			allocated_list = temp;
			//Reduce freelist size by advancing it further down memory
			(*freelist).addr = (*freelist).addr + nbytes;
			(*freelist).size = (*freelist).size - nbytes;
			done = 1;
		}	
	//Block is the exact size we want
		else if (nbytes == (*freelist).size){
			struct block *x = freelist;
			//Check if you've traversed to the middle of the list
			if (counter > 0){
				(*prev).next = (*freelist).next;
			}
			else{
				freelist = (*freelist).next;
			}
			(*x).next = allocated_list;
			allocated_list = x;
			x = NULL;
			done = 1;
		}
		//Check next block
		else if((*freelist).next != NULL){
			prev = freelist;
			freelist = (*freelist).next;
		}
		//End of freelist
		else if((*freelist).next == NULL){
			done = -1;
		}
		counter++;
	}
	freelist = header; //Restore reference to the original head
	prev = NULL;
	if (done == -1){
		return NULL;
	}
	else{
		return (*allocated_list).addr;
	}
}


int sfree(void *addr) {
	done = 1; // 0 if successful, -1 if failed
	counter = 0;
	header = allocated_list; //Save head of list
	while(done == 1){
		//Found the block you want to free
		if(addr == (*allocated_list).addr){
			struct block *headfree = freelist;
			struct block *x = allocated_list;
			struct block *prevfree;
			//Check if the block is in the middle of the allocated_list
			if (counter > 0){
				(*prev).next = (*allocated_list).next;
			}
			else{
				allocated_list = (*allocated_list).next;
			}
			int insertMiddle = 0;
			//Insert the allocated block into the freelist
			while(done == 1){
				//Ensure the blocks are in increasing address order
				//Try to insert block in the next freelist node
				if(addr > (*freelist).addr){
					prevfree = freelist;
					freelist = (*freelist).next;
					insertMiddle = 1;
				}
				//Insert the block into the middle of  freelist
				else if(insertMiddle == 1){
					done = 0;
					(*x).next = freelist;
					(*prevfree).next = x;
					freelist = headfree;
				}
				//Prepend the block normally to freelist
				else if(insertMiddle == 0){
					(*x).next = freelist;
					freelist = x;
					done = 0;
				}
			}
			x = NULL;
		}
		//Check next block
		else if((*allocated_list).next != NULL){
			prev = allocated_list;
			allocated_list = (*allocated_list).next;
		}
		//End of allocated_list
		else if((*allocated_list).next == NULL){
			done = -1;
		}
	counter++;	
	}
	//Restore reference to head of linked list
	allocated_list = header;
	header = NULL;
    return done;
}


/* Initialize the memory space used by smalloc,
 * freelist, and allocated_list
 * Note:  mmap is a system call that has a wide variety of uses.  In our
 * case we are using it to allocate a large region of memory. 
 * - mmap returns a pointer to the allocated memory
 * Arguments:
 * - NULL: a suggestion for where to place the memory. We will let the 
 *         system decide where to place the memory.
 * - PROT_READ | PROT_WRITE: we will use the memory for both reading
 *         and writing.
 * - MAP_PRIVATE | MAP_ANON: the memory is just for this process, and 
 *         is not associated with a file.
 * - -1: because this memory is not associated with a file, the file 
 *         descriptor argument is set to -1
 * - 0: only used if the address space is associated with a file.
 */
void mem_init(int size) {
    mem = mmap(NULL, size,  PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON, -1, 0);
    if(mem == MAP_FAILED) {
         perror("mmap");
         exit(1);
    }
	else{
		//malloc enough size to store linked list
		freelist = malloc(sizeof(struct block));
		allocated_list = NULL;
		//Point address to memory pool provided by mmap
		(*freelist).addr = mem;
		(*freelist).size = size;
		(*freelist).next = NULL;
	}
}
void free_linked_list(struct block* list){
	struct block* a;
	//Traverse through list and free each block
	while (list != NULL){
	   a = list;
	   list = (*list).next;
	   free(a);
	   a = NULL;
	}
}
void mem_clean(){
	free_linked_list(freelist);
	free_linked_list(allocated_list);
}

